//
// Created by b-erhart on 13.04.21.
//

#ifndef COMPUTERGRAPHICS_RASTERIZATION_HPP
#define COMPUTERGRAPHICS_RASTERIZATION_HPP

#include <algorithm>
#include <vector>
#include <limits>
#include <cmath>

#include "../simple2d.hpp"
#include "../types/coordinates.hpp"
#include "../types/line.hpp"
#include "../exercise01/midpoint_line.hpp"

void rasterizeRectangle(Coordinate2d<int> a, Coordinate2d<int> b) {
    Coordinate2d<int> topLeft{ std::min(a.x, b.x), std::min(a.y, b.y) };
    Coordinate2d<int> bottomRight{ std::max(a.x, b.x), std::max(a.y, b.y) };

    for (int i{ topLeft.y }; i <= bottomRight.y; ++i) {
        for (int j{ topLeft.x }; j <= bottomRight.x; j++) {
            setPixel(j, i);
        }
    }
}

void rasterizeTriangle(Coordinate2d<int> a, Coordinate2d<int> b, Coordinate2d<int> c) {
    int xMin = std::min(a.x, std::min(b.x, c.x));
    int yMin = std::min(a.y, std::min(b.y, c.y));
    int xMax = std::max(a.x, std::max(b.x, c.x));
    int yMax = std::max(a.y, std::max(b.y, c.y));

    int f0 = (a.y - b.y) * (xMin - a.x) + (b.x - a.x) * (yMin - a.y);
    int f1 = (b.y - c.y) * (xMin - b.x) + (c.x - b.x) * (yMin - b.y);
    int f2 = (c.y - a.y) * (xMin - c.x) + (a.x - c.x) * (yMin - c.y);

    for (int i{ yMin }; i <= yMax; ++i) {
        int ff0 = f0;
        int ff1 = f1;
        int ff2 = f2;

        for (int j{ xMin }; j <= xMax; ++j) {
            if (ff0 >= 0 && ff1 >= 0 && ff2 >= 0) {
                setPixel(j, i);
            }

            ff0 = ff0 + (a.y - b.y);
            ff1 = ff1 + (b.y - c.y);
            ff2 = ff2 + (c.y - a.y);
        }

        f0 = f0 + (b.x - a.x);
        f1 = f1 + (c.x - b.x);
        f2 = f2 + (a.x - c.x);
    }

    drawMidpointLine(a.x, a.y, b.x, b.y);
    drawMidpointLine(b.x, b.y, c.x, c.y);
    drawMidpointLine(c.x, c.y, a.x, a.y);
}

void rasterizePolygon(const std::vector<Coordinate2d<int>>& corners) {
    std::vector<Line> passiveEdges{ };
    std::vector<Line> activeEdges{ };
    int yMin{ std::numeric_limits<int>::max() };
    int yMax{ std::numeric_limits<int>::min() };

    for (int i{ 0 }; i < corners.size(); ++i) {
        Line newEdge{ corners[i], corners[i < corners.size() - 1 ? i + 1 : 0] };

        passiveEdges.emplace_back(newEdge);

        yMin = std::min(yMin, newEdge.getYMin());
        yMax = std::max(yMax, newEdge.getYMax());
    }

    auto oldColor = getDrawingColor();
    setDrawingColor(1, 0, 0);

    for (auto edge : passiveEdges) {
        edge.drawWithMidpoint();
    }

    setDrawingColor(oldColor);

    for (int j{ yMin }; j <= yMax; ++j) {
        for (auto currentEdge = passiveEdges.begin(); currentEdge != passiveEdges.end(); ) {
            if (currentEdge->getYMin() != j) {
                currentEdge++;
                continue;
            }

            activeEdges.emplace_back(*currentEdge);
            currentEdge = passiveEdges.erase(currentEdge);
        }

        for (auto currentEdge = activeEdges.begin(); currentEdge != activeEdges.end(); ) {
            if (currentEdge->getYMax() != j) {
                currentEdge++;
                continue;
            }

            currentEdge = activeEdges.erase(currentEdge);
        }

        std::sort(activeEdges.begin(), activeEdges.end(), Line::greater);

        for (int k{ 0 }; k < activeEdges.size( ); k += 2) {
            for (int l { static_cast<int>(std::round(activeEdges[k].getXs())) }; l <= static_cast<int>(std::round(activeEdges[k + 1].getXs())); l++) {
                setPixel(l, j);
            }
        }

        for (Line &currentEdge : activeEdges) {
            currentEdge.incrementXs();
        }
    }

    /*for (int l{ 0 }; l < corners.size(); ++l) {
        Coordinate2d<int> otherCorner = corners[l < corners.size() - 1 ? l + 1 : 0];
        drawMidpointLine(corners[l].x, corners[l].y, otherCorner.x, otherCorner.y);
    }*/
}

#endif // #ifndef COMPUTERGRAPHICS_RASTERIZATION_HPP
