//
// Created by b-erhart on 13.04.21.
//

#include <cmath>
#include <iostream>
#include <vector>
#include <eigen3/Eigen/Dense> // sudo xbps-install -S eigen

#include "../simple2d.hpp"
#include "rasterization.hpp"
#include "catmull_rom_spline.hpp"

using Eigen::Vector2f;
using Eigen::Matrix3f;

void drawPolygonRaserization() {
    std::vector<Coordinate2d<int>> corners;

    corners.emplace_back(Coordinate2d<int>{ 30, 30 });
    corners.emplace_back(Coordinate2d<int>{ 80, 50 });
    corners.emplace_back(Coordinate2d<int>{ 120, 35 });
    corners.emplace_back(Coordinate2d<int>{ 200, 55 });
    corners.emplace_back(Coordinate2d<int>{ 160, 90 });
    corners.emplace_back(Coordinate2d<int>{ 180, 150 });
    corners.emplace_back(Coordinate2d<int>{ 100, 180 });
    corners.emplace_back(Coordinate2d<int>{ 90, 90 });
    corners.emplace_back(Coordinate2d<int>{ 65, 110 });

    setDrawingColor(0.5, 1, 0.25);

    rasterizePolygon(corners);

    resetDrawingColor();
}

void drawCatmullRomSpline() {
    std::vector<Coordinate2d<int>> controlPoints;
    controlPoints.emplace_back(10, 80);
    controlPoints.emplace_back(50, 100);
    controlPoints.emplace_back(100, 80);
    controlPoints.emplace_back(150, 110);
    controlPoints.emplace_back(200, 70);
    controlPoints.emplace_back(230, 100);

    setDrawingColor(0.25, 1, 0.75);

    for (int i{ 1 }; i < controlPoints.size() - 2; ++i) {
        for (double t{ 0 }; t <= 1; t += 0.1) {
            Coordinate2d<int> linePoint = calculateCatmullRom(controlPoints[i - 1], controlPoints[i],
                                                              controlPoints[i + 1], controlPoints[i + 2], t);
            setPixel(linePoint.x, linePoint.y);
        }
    }

    setDrawingColor(1, 0.25, 0.75);

    for (auto &controlPoint : controlPoints) {
        setPixel(controlPoint.x, controlPoint.y);
    }

    resetDrawingColor();
}

void drawPythagoreanTheorem(float width, float height, float scaling) {
    const float offset = std::max(width * scaling, height * scaling) + 10;

    Vector2f triangle[3];
    triangle[0] = Vector2f(0, 0);
    triangle[1] = Vector2f(width, 0);
    triangle[2] = Vector2f(0, height);

    Vector2f square[4];
    square[0] = Vector2f(0, 0);
    square[1] = Vector2f(1, 0);
    square[2] = Vector2f(1, 1);
    square[3] = Vector2f(0, 1);

    Matrix3f triangleTranslation = getTranslationMatrix(offset, offset);
    Matrix3f triangleScaling = getScalingMatrix(scaling, scaling);
    Matrix3f triangleRotation = getRotationMatrix(0);

    for (auto &corner : triangle) {
        corner = transform(corner, triangleTranslation, triangleScaling, triangleRotation);
    }

    setDrawingColor(0, 0.5, 1);
    rasterizeTriangle(triangle[0], triangle[1], triangle[2]);

    setDrawingColor(1, 0, 0);
    for (int i = 0; i < 3; i++) {
        const Vector2f &lineStart = triangle[i];
        const Vector2f &lineEnd = triangle[i < 2 ? i + 1 : 0];

        float lineLength = sqrt(pow(lineStart(0) - lineEnd(0), 2) + pow(lineStart(1) - lineEnd(1), 2));

        Matrix3f squareTranslation = getTranslationMatrix(lineStart(0), lineStart(1));
        Matrix3f squareScaling = getScalingMatrix(lineLength, lineLength);
        Matrix3f squareRotation;

        if (lineStart(0) != lineEnd(0) && lineStart(1) != lineEnd(1)) {
            squareRotation = getRotationMatrix(atan2(lineEnd(1) - lineStart(1), lineEnd(0) - lineStart(0)) - M_PI / 2);
        }
        else if (lineStart(0) == lineEnd(0)) {
            squareRotation = getRotationMatrix(M_PI);
        }
        else {
            squareRotation = getRotationMatrix(-M_PI/2);
        }

        for (auto &corner : square) {
            Vector2f transformedCorner = transform(corner, squareTranslation, squareScaling, squareRotation);
            setPixel(transformedCorner(0), transformedCorner(1));
        }
    }
}

int main(int argc, char *argv[]) {
    setupGlut(&argc, argv, "[float] Exercise 3", 1024, 1024);

    std::string selectedExercise{ };
    while (true) {
        std::cout << "Please select an exercise:" << std::endl;
        std::cout << "1a - Rasterization of axis-parallel rectangles" << std::endl;
        std::cout << "1b - Rasterization of triangles" << std::endl;
        std::cout << "1c - Rasterization of any polygon (scanline)" << std::endl;
        std::cout << "2a - Catmull Rom spline" << std::endl;
        std::cout << "3c - Transformations" << std::endl;

        std::cout << "Selection: ";
        std::cin >> selectedExercise;

        if (selectedExercise == "1a") {
            setDrawingColor(1, 0, 1);

            rasterizeRectangle(Coordinate2d<int>{ 50, 200 }, Coordinate2d<int>{ 200, 80 });

            resetDrawingColor();
            break;
        }
        else if (selectedExercise == "1b") {
            setDrawingColor(0, 0.5, 1);

            rasterizeTriangle(Coordinate2d<int>{ 30, 50 }, Coordinate2d<int>{ 220, 90 }, Coordinate2d<int>{ 150, 200 });

            resetDrawingColor();
            break;
        }
        else if (selectedExercise == "1c") {
            drawPolygonRaserization();
            break;
        }
        else if (selectedExercise == "2a") {
            drawCatmullRomSpline();
            break;
        }
        else if (selectedExercise == "3c") {
            //drawPythagoreanTheoremOld();
            drawPythagoreanTheorem(4, 3, 10);
            break;
        }

        std::cout << R"(Invalid selection. Please type either "1a", "1b", "1c", "2a" or "3c")" << std::endl
                  << std::endl;
    }

    glutMainLoop();

    return 0;
}