//
// Created by b-erhart on 13.04.21.
//

#ifndef COMPUTERGRAPHICS_CATMULL_ROM_SPLINE_HPP
#define COMPUTERGRAPHICS_CATMULL_ROM_SPLINE_HPP

#include "../simple2d.hpp"
#include "../types/coordinates.hpp"

Coordinate2d<int>
calculateCatmullRom(Coordinate2d<int> p0, Coordinate2d<int> p1, Coordinate2d<int> p2, Coordinate2d<int> p3, double t) {
    Coordinate2d<int> result{ };

    result.x = 0.5 * ( (2 * p1.x) +
                       (-1 * p0.x + p2.x) * t +
                       (2 * p0.x - 5 * p1.x + 4 * p2.x - p3.x) * t * t +
                       (-1 * p0.x + 3 * p1.x - 3 * p2.x + p3.x) * t * t * t ) + 0.5;

    result.y = 0.5 * ( (2 * p1.y) +
                       (-1 * p0.y + p2.y) * t +
                       (2 * p0.y - 5 * p1.y + 4 * p2.y - p3.y) * t * t +
                       (-1 * p0.y + 3 * p1.y - 3 * p2.y + p3.y) * t * t * t ) + 0.5;

    return result;
}

#endif // #ifndef COMPUTERGRAPHICS_CATMULL_ROM_SPLINE_HPP
