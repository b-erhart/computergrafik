//
// Created by b-erhart on 24.03.21.
//

#ifndef COMPUTERGRAPHICS_SIMPLE2D_HPP
#define COMPUTERGRAPHICS_SIMPLE2D_HPP

#include <GL/glew.h>            // Glew (sudo xbps-install -S glew glew-devel)
#include <GL/glut.h>            // Glut (sudo xbps-install -S libfreeglut libfreeglut-devel)

inline const int RESOLUTION{ 256 };

struct RgbPixel {
    GLfloat R, G, B;

    explicit RgbPixel(GLfloat r = 0, GLfloat g = 0, GLfloat b = 0) : R(r), G(g), B(b) { };
};

inline RgbPixel drawingColor{ GLfloat{ 1 }, GLfloat{ 1 }, GLfloat{ 1 } };

inline void setDrawingColor(GLfloat r, GLfloat g, GLfloat b) {
    drawingColor = RgbPixel{ r, g, b };
}

inline void setDrawingColor(RgbPixel rgb) {
    drawingColor = rgb;
}

inline RgbPixel getDrawingColor() {
    return drawingColor;
}

inline void resetDrawingColor() {
    drawingColor = RgbPixel{ GLfloat{ 1 }, GLfloat{ 1 }, GLfloat{ 1 } };
}

inline RgbPixel framebuffer[RESOLUTION][RESOLUTION];

inline void setPixel(int x, int y, GLfloat r, GLfloat g, GLfloat b) {
    framebuffer[x < 0 ? 0 : (x >= RESOLUTION ? RESOLUTION - 1 : x)][y < 0 ? 0 : (y >= RESOLUTION ? RESOLUTION - 1 : y)] = RgbPixel(r, g, b);
    glutPostRedisplay();
}

inline void setPixel(int x, int y) {
    setPixel(x, y, drawingColor.R, drawingColor.G, drawingColor.B);
}

inline void renderScene() {
    glClear(GL_COLOR_BUFFER_BIT); // Clear the window with current clearing color

    GLfloat d{ 2.0f / GLfloat(RESOLUTION) };
    for (int y{ 0 }; y < RESOLUTION; y++)
        for (int x{ 0 }; x < RESOLUTION; x++) {
            const RgbPixel &p{ framebuffer[x][y] };

            if (p.R == 0 && p.G == 0 && p.B == 0)
                continue;

            glColor3f(p.R, p.G, p.B);

            GLfloat vpx{ GLfloat(x) * 2.0f / GLfloat(RESOLUTION) - 1.0f };
            GLfloat vpy{ GLfloat(y) * 2.0f / GLfloat(RESOLUTION) - 1.0f };

            glRectf(vpx, vpy, vpx + d, vpy + d);
        }

    glFlush(); // Flush drawing commands
}

inline void setupRc() {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

inline void keyPress(unsigned char key, int x, int y) {
    switch (key) {
        case 'q':
            exit(1);
    }
}

inline void setupGlut(int *argc, char **argv, const char *windowTitle, int windowWidth, int windowHeight) {
    glutInit(argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
    glutCreateWindow(windowTitle);
    glutReshapeWindow(windowHeight, windowWidth);
    glutDisplayFunc(renderScene);
    glutKeyboardFunc(keyPress);

    setupRc();
}

#endif // #ifndef COMPUTERGRAPHICS_SIMPLE2D_HPP
