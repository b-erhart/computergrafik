//
// Created by b-erhart on 25.03.21.
//

#ifndef COMPUTERGRAPHICS_PPM_EXPORT_HPP
#define COMPUTERGRAPHICS_PPM_EXPORT_HPP

#include <fstream>
#include "../simple2d.hpp"

void exportPpmFile(const std::string& filename) {
    std::ofstream outputFile;
    outputFile.open(filename + ".ppm", std::ios_base::binary);

    outputFile << "P3" << std::endl;
    outputFile << RESOLUTION << " " << RESOLUTION << std::endl;
    outputFile << "255" << std::endl;

    for (int y{ RESOLUTION - 1 }; y >= 0; y--) {
        for (int x{ 0 }; x < RESOLUTION; x++) {
            int intR = GLint(framebuffer[x][y].R * 255);
            int intG = GLint(framebuffer[x][y].G * 255);
            int intB = GLint(framebuffer[x][y].B * 255);

            outputFile << intR << " " << intG << " " << intB << "   ";
        }
        outputFile << std::endl;
    }

    outputFile.close();
}

#endif // #ifndef COMPUTERGRAPHICS_PPM_EXPORT_HPP