//
// Created by b-erhart on 25.03.21.
//

#ifndef COMPUTERGRAPHICS_BRESENHAM_LINE_HPP
#define COMPUTERGRAPHICS_BRESENHAM_LINE_HPP

#include <algorithm>

#include "../simple2d.hpp"
#include "../types/coordinate2f.hpp"
#include "../exercise04/clipping.hpp"

inline void drawBresenhamLine(int x1, int y1, int x2, int y2) {
    if (calculateOutcode(x1, y1) != 0 && calculateOutcode(x2, y2) != 0) {
        return;
    }

    if (x2 < x1) {
        std::swap(x1, x2);
        std::swap(y1, y2);
    }

    int dx{ x2 - x1 };
    int dy{ y2 - y1 };
    int dxTwice{ dx + dx };
    int dyTwice{ dy + dy };

    if (abs(dx) > abs(dy) && dy >= 0) {
        int error{ dyTwice - dx };

        while (x1 <= x2) {
            if (calculateOutcode(x1, y1) == 0) {
                setPixel(x1, y1);
            }

            x1++;

            error += dyTwice;

            if (error > 0) {
                y1++;
                error -= dxTwice;
            }
        }
    }
    else if (abs(dx) > abs(dy)) {
        int error{ dyTwice + dx };

        while (x1 <= x2) {
            if (calculateOutcode(x1, y1) == 0) {
                setPixel(x1, y1);
            }

            x1++;

            error += dyTwice;

            if (error < 0) {
                y1--;
                error += dxTwice;
            }
        }
    }
    else if (dy > 0) {
        int error{ dxTwice - dy };

        while (y1 <= y2) {
            if (calculateOutcode(x1, y1) == 0) {
                setPixel(x1, y1);
            }

            y1++;

            error += dxTwice;

            if (error > 0) {
                x1++;
                error -= dyTwice;
            }
        }
    }
    else {
        int error{ dxTwice + dy };

        while (y2 <= y1) {
            if (calculateOutcode(x1, y1) == 0) {
                setPixel(x1, y1);
            }

            y1--;

            error += dxTwice;

            if (error > 0) {
                x1++;
                error += dyTwice;
            }
        }
    }
}

inline void drawBresenhamLine(Coordinate2f start, Coordinate2f end) {
    drawBresenhamLine(start.getIntX(), start.getIntY(), end.getIntX(), end.getIntY());
}

#endif // #ifndef COMPUTERGRAPHICS_BRESENHAM_LINE_HPP