//
// Created by b-erhart on 25.03.21.
//

#ifndef COMPUTERGRAPHICS_MIDPOINT_LINE_HPP
#define COMPUTERGRAPHICS_MIDPOINT_LINE_HPP

#include "../simple2d.hpp"
#include "../types/coordinate2f.hpp"
#include "../exercise04/clipping.hpp"

inline void drawMidpointLine(int x1, int y1, int x2, int y2) {
    if (calculateOutcode(x1, y1) != 0 && calculateOutcode(x2, y2) != 0) {
        return;
    }

    if (x2 < x1) {
        std::swap(x1, x2);
        std::swap(y1, y2);
    }

    int x{ x1 };
    int y{ y1 };
    int dx{ x2 - x1 };
    int dy{ y2 - y1 };

    if (abs(dx) > abs(dy) && dy >= 0) {
        int f{ dy - dx / 2 };

        for (int i{ 1 }; i <= dx; i++) {
            if (calculateOutcode(x, y) == 0) {
                setPixel(x, y);
            }

            x++;

            if (f > 0) {
                y += 1;
                f -= dx;
            }

            f += dy;
        }
    }
    else if (abs(dx) > abs(dy)) {
        int f{ dy + dx / 2 };

        for (int i{ 1 }; i <= dx; i++) {
            if (calculateOutcode(x, y) == 0) {
                setPixel(x, y);
            }

            x++;

            if (f < 0) {
                y -= 1;
                f += dx;
            }

            f += dy;
        }
    }
    else if (dy > 0) {
        int f{ dx - dy / 2 };

        for (int i{ 1 }; i <= dy; i++) {
            if (calculateOutcode(x, y) == 0) {
                setPixel(x, y);
            }

            y++;

            if (f > 0) {
                x += 1;
                f -= dy;
            }

            f += dx;
        }
    }
    else {
        int f{ dx + dy / 2 };
        for (int i{ 1 }; i <= abs(dy); i++) {
            if (calculateOutcode(x, y) == 0) {
                setPixel(x, y);
            }

            y--;

            if (f > 0) {
                x += 1;
                f -= abs(dy);
            }

            f += dx;
        }
    }
}

inline void drawMidpointLine(Coordinate2f start, Coordinate2f end) {
    drawMidpointLine(start.getIntX(), start.getIntY(), end.getIntX(), end.getIntY());
}

#endif // #ifndef COMPUTERGRAPHICS_MIDPOINT_LINE_HPP