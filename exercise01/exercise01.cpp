//
// Created by b-erhart on 25.03.21.
//

#include "../simple2d.hpp"
#include "bresenham_line.hpp"
#include "midpoint_line.hpp"
#include "ppm_export.hpp"

int main(int argc, char *argv[]) {
    setupGlut(&argc, argv, "[float] Exercise 1", 1024, 1024);

    // draw diagonal rainbow line
    for (int n = 0; n < RESOLUTION; n++)
        setPixel(n, n, GLfloat(n) / RESOLUTION, 0, 1.0f - GLfloat(n) / RESOLUTION);

    // draw random single pixels
    for (int i = 0; i < 50; i++) {
        int randX{ rand() % RESOLUTION };
        int randY{ rand() % RESOLUTION };
        setPixel(randX, randY, GLfloat(1), GLfloat(1), GLfloat(1));
    }

    // draw bresenham lines
    setDrawingColor(0, 1, 1);
    Coordinate2f bCenter = Coordinate2f(100, 150);
    drawBresenhamLine(bCenter, Coordinate2f(125, 200));
    drawBresenhamLine(bCenter, Coordinate2f(150, 175));
    drawBresenhamLine(bCenter, Coordinate2f(150, 125));
    drawBresenhamLine(bCenter, Coordinate2f(125, 100));
    drawBresenhamLine(bCenter, Coordinate2f( 75, 200));
    drawBresenhamLine(bCenter, Coordinate2f( 50, 175));
    drawBresenhamLine(bCenter, Coordinate2f( 50, 125));
    drawBresenhamLine(bCenter, Coordinate2f( 75, 100));

    // draw midpoint lines
    setDrawingColor(0.5, 0, 1);
    Coordinate2f mCenter = Coordinate2f(200, 50);
    drawMidpointLine(mCenter, Coordinate2f(225, 100));
    drawMidpointLine(mCenter, Coordinate2f(250,  75));
    drawMidpointLine(mCenter, Coordinate2f(250,  25));
    drawMidpointLine(mCenter, Coordinate2f(225,   0));
    drawMidpointLine(mCenter, Coordinate2f(175, 100));
    drawMidpointLine(mCenter, Coordinate2f(150,  75));
    drawMidpointLine(mCenter, Coordinate2f(150,  25));
    drawMidpointLine(mCenter, Coordinate2f(175,   0));

    exportPpmFile("image_exercise01");

    glutMainLoop();

    return 0;
}