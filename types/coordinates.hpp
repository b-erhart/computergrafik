//
// Created by b-erhart on 03.04.21.
//

#ifndef COMPUTERGRAPHICS_COORDINATES_HPP
#define COMPUTERGRAPHICS_COORDINATES_HPP

#include <algorithm>
#include <cmath>
#include <eigen3/Eigen/Dense> // sudo dnf install eigen3-devel

using Eigen::Vector2f;
using Eigen::Vector3f;
using Eigen::Matrix3f;

template <typename T>
struct HomogeneousCoordinateOld;

template<typename T>
struct Coordinate2d {
    T x{ };
    T y{ };

    Coordinate2d() {
        x = 0;
        y = 0;
    }

    Coordinate2d(T x, T y) : x(x), y(y) { };

    Coordinate2d(Vector2f vector) {
        x = vector(0);
        y = vector(1);
    }

    HomogeneousCoordinateOld<T> toHomogeneous() {
        return HomogeneousCoordinateOld<T>{ x, y, 1 };
    }
};

inline Vector2f convertToCartesian(const Vector3f& homogeneous) {
    return Vector2f(homogeneous(0) / homogeneous(2), homogeneous(1) / homogeneous(2));
}

inline Vector3f convertToHomogeneous(const Vector2f& cartesian) {
    return Vector3f(cartesian(0), cartesian(1), 1);
}

inline Matrix3f getTranslationMatrix(float tx, float ty) {
    Matrix3f translationMatrix = Matrix3f::Identity();

    translationMatrix(0, 2) = tx;
    translationMatrix(1, 2) = ty;

    return translationMatrix;
}

inline Matrix3f getScalingMatrix(float sx, float sy) {
    Matrix3f scalingMatrix = Matrix3f::Identity();

    scalingMatrix(0, 0) = sx;
    scalingMatrix(1, 1) = sy;

    return scalingMatrix;
}

inline Matrix3f getRotationMatrix(float angle) {
    Matrix3f rotationMatrix = Matrix3f::Identity();

    rotationMatrix(0, 0) = cos(angle);
    rotationMatrix(0, 1) = -sin(angle);
    rotationMatrix(1, 0) = sin(angle);
    rotationMatrix(1, 1) = cos(angle);

    return rotationMatrix;
}

inline Vector2f transform(Vector2f point, Matrix3f translation, Matrix3f scaling, Matrix3f rotation) {
    Vector3f transformedHomogeneous = translation * scaling * rotation * convertToHomogeneous(point);

    return convertToCartesian(transformedHomogeneous);
}

inline float getX(Vector2f vector) {
    return vector(0);
}

inline float getY(Vector2f vector) {
    return vector(1);
}

#endif // #ifndef COMPUTERGRAPHICS_COORDINATES_HPP
