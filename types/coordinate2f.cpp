//
// Created by b-erhart on 03.05.21.
//

#include <utility>
#include <cmath>
#include "coordinate2f.hpp"

Coordinate2f::Coordinate2f(Vector2f vector2f) {
    this->vector = std::move(vector2f);
}

Coordinate2f::Coordinate2f(float x, float y) {
    this->vector = Vector2f(x, y);
}

float Coordinate2f::getX() {
    return this->vector(0);
}

int Coordinate2f::getIntX() {
    return static_cast<int>(std::round(vector(0)));
}

void Coordinate2f::setX(float x) {
    this->vector(0) = x;
}

float Coordinate2f::getY() {
    return this->vector(1);
}

int Coordinate2f::getIntY() {
    return static_cast<int>(std::round(vector(1)));
}

void Coordinate2f::setY(float y) {
    this->vector(1) = y;
}

Vector2f& Coordinate2f::asVector() {
    return this->vector;
}