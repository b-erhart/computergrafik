//
// Created by b-erhart on 20.04.21.
//

#ifndef COMPUTERGRAPHICS_LINE_HPP
#define COMPUTERGRAPHICS_LINE_HPP

#include "coordinates.hpp"

class Line {
private:
    Coordinate2d<int> a;
    Coordinate2d<int> b;
    double xs{ 0 };
    double deltaX{ 0 };

public:
    Line(Coordinate2d<int> a, Coordinate2d<int> b);

    int getYMin();
    int getYMax();
    double getXs();
    void incrementXs();
    double getDeltaX();
    static bool greater(Line a, Line b);
    void drawWithMidpoint();
    void drawWithBresenham();
};

#endif //COMPUTERGRAPHICS_LINE_HPP
