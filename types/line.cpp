//
// Created by b-erhart on 20.04.21.
//

#include "line.hpp"
#include "../exercise01/midpoint_line.hpp"
#include "../exercise01/bresenham_line.hpp"

Line::Line(Coordinate2d<int> a, Coordinate2d<int> b) : a(a), b(b) {
    Coordinate2d<int> max = a.y >= b.y ? a : b;
    Coordinate2d<int> min = a.y < b.y ? a : b;

    xs = min.x;
    deltaX = (1.0 * max.x - 1.0 * min.x) / (1.0 * max.y - 1.0 * min.y);
}

int Line::getYMin() {
    return std::min(a.y, b.y);
}

int Line::getYMax() {
    return std::max(a.y, b.y);
}

double Line::getXs() {
    return xs;
}

void Line::incrementXs() {
    xs += deltaX;
}

double Line::getDeltaX() {
    return deltaX;
}

bool Line::greater(Line a, Line b) {
    return a.xs < b.xs;
}

void Line::drawWithMidpoint() {
    drawMidpointLine(a.x, a.y, b.x, b.y);
}

void Line::drawWithBresenham() {
    drawBresenhamLine(a.x, a.y, b.x, b.y);
}