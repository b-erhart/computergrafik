//
// Created by b-erhart on 03.05.21.
//

#ifndef COMPUTERGRAPHICS_COORDINATE2F_HPP
#define COMPUTERGRAPHICS_COORDINATE2F_HPP

#include <eigen3/Eigen/Dense> // sudo dnf install eigen3-devel

using Eigen::Vector2f;

class Coordinate2f {
private:
    Vector2f vector;

public:
    Coordinate2f(Vector2f vector2f);

    Coordinate2f(float x, float y);

    float getX();

    int getIntX();

    void setX(float x);

    float getY();

    int getIntY();

    void setY(float y);

    Vector2f &asVector();
};


#endif //COMPUTERGRAPHICS_COORDINATE2F_HPP
