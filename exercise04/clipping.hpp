//
// Created by b-erhart on 26.04.21.
//

#ifndef COMPUTERGRAPHICS_CLIPPING_HPP
#define COMPUTERGRAPHICS_CLIPPING_HPP

#include <vector>
#include <bitset>
#include <eigen3/Eigen/Dense>

#include "../simple2d.hpp"

using Eigen::Vector2f;

inline std::bitset<4> calculateOutcode(int x, int y) {
    int c = 0b0000;

    if (x < 0) {
        c = c | 0b1000;
    }

    if (x > RESOLUTION - 1) {
        c = c | 0b0100;
    }

    if (y < 0) {
        c = c | 0b0010;
    }

    if (y > RESOLUTION - 1) {
        c = c | 0b0001;
    }

    return c;
}

inline std::bitset<4> calculateOutcode(const Vector2f& point) {
    return calculateOutcode(point(0), point(1));
}

inline Vector2f calculateCuttingPoint(const Vector2f& inside, const Vector2f& outside) {
    const std::bitset<4> outcodeOutside = calculateOutcode(outside);

    if (inside(0) == outside(0) && outcodeOutside.test(1)) {
        return Vector2f(inside(0), 0);
    }
    else if (inside(0) == outside(0) && outcodeOutside.test(0)) {
        return Vector2f(inside(0), RESOLUTION - 1);
    }

    float m = (inside(1) - outside(1)) / (inside(0) - outside(0));
    float t = inside(1) - m * inside(0);

    float cutTopX = (RESOLUTION - 1 - t) / m;
    float cutRightY = m * (RESOLUTION - 1) + t;
    float cutBottomX = (0 - t) / m;
    float cutLeftY = t;

    if (outcodeOutside.test(0) && cutTopX >= 0 && cutTopX < RESOLUTION) {
        return Vector2f(cutTopX, RESOLUTION - 1);
    }
    else if (outcodeOutside.test(1) && cutBottomX >= 0 && cutBottomX < RESOLUTION) {
        return Vector2f(cutBottomX, 0);
    }
    else if (outcodeOutside.test(2) && cutRightY >= 0 && cutRightY < RESOLUTION) {
        return Vector2f(RESOLUTION - 1, cutRightY);
    }
    else if (outcodeOutside.test(3) && cutLeftY >= 0 && cutLeftY < RESOLUTION) {
        return Vector2f(0, cutLeftY);
    }

    return outside;
}

inline std::vector<Vector2f> calculateClippedPolygonCorners(const std::vector<Vector2f>& polygonCorners) {
    std::vector<Vector2f> outputPolygon;

    for (int i = 0; i < polygonCorners.size(); i++) {
        const Vector2f& start = polygonCorners[i];
        const Vector2f& end = polygonCorners[i < polygonCorners.size() - 1 ? i + 1 : 0];

        const std::bitset<4> outcodeStart = calculateOutcode(start);
        const std::bitset<4> outcodeEnd = calculateOutcode(end);

        if (outcodeStart != 0 && outcodeEnd != 0) {
            continue;
        }
        else if (outcodeStart != 0 && outcodeEnd == 0) {
            outputPolygon.emplace_back(calculateCuttingPoint(end, start));
            outputPolygon.emplace_back(end);
        }
        else if (outcodeStart == 0 && outcodeEnd == 0) {
            outputPolygon.emplace_back(end);
        }
        else {
            outputPolygon.emplace_back(calculateCuttingPoint(start, end));
        }
    }

    return outputPolygon;
};

#endif // #ifndef COMPUTERGRAPHICS_CLIPPING_HPP
