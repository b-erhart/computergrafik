//
// Created by b-erhart on 26.04.21.
//

#ifndef COMPUTERGRAPHICS_BARYCENTRIC_TRIANGLE_HPP
#define COMPUTERGRAPHICS_BARYCENTRIC_TRIANGLE_HPP

#include "../simple2d.hpp"
#include "../types/coordinates.hpp"

void rasterizeBarycentricTriangle(Vector2f a, Vector2f b, Vector2f c) {
    int xMin = std::min(getX(a), std::min(getX(b), getX(c)));
    int yMin = std::min(getY(a), std::min(getY(b), getY(c)));
    int xMax = std::max(getX(a), std::max(getX(b), getX(c)));
    int yMax = std::max(getY(a), std::max(getY(b), getY(c)));

    int f0 = (getY(a) - getY(b)) * (xMin - getX(a)) + (getX(b) - getX(a)) * (yMin - getY(a));
    int f1 = (getY(b) - getY(c)) * (xMin - getX(b)) + (getX(c) - getX(b)) * (yMin - getY(b));
    int f2 = (getY(c) - getY(a)) * (xMin - getX(c)) + (getX(a) - getX(c)) * (yMin - getY(c));

    for (int i{ yMin }; i <= yMax; ++i) {
        int ff0 = f0;
        int ff1 = f1;
        int ff2 = f2;

        for (int j{ xMin }; j <= xMax; ++j) {
            double d = ff0 + ff1 + ff2;

            if (ff0 >= 0 && ff1 >= 0 && ff2 >= 0) {
                setPixel(j, i, ff0 / d, ff1 / d, ff2 / d);
            }

            ff0 = ff0 + (getY(a) - getY(b));
            ff1 = ff1 + (getY(b) - getY(c));
            ff2 = ff2 + (getY(c) - getY(a));
        }

        f0 = f0 + (getX(b) - getX(a));
        f1 = f1 + (getX(c) - getX(b));
        f2 = f2 + (getX(a) - getX(c));
    }
}

#endif // #ifndef COMPUTERGRAPHICS_BARYCENTRIC_TRIANGLE_HPP
