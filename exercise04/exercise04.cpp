//
// Created by b-erhart on 26.04.21.
//

#include <iostream>

#include "../simple2d.hpp"
#include "clipping.hpp"
#include "barycentric_triangle.hpp"
#include "../exercise01/bresenham_line.hpp"
#include "../exercise01/midpoint_line.hpp"

void demonstrateCohanSutherland() {
    drawBresenhamLine(50, 100, RESOLUTION + 20, 200);
    drawMidpointLine(150, 50, 200, -50);
    drawBresenhamLine(20, -10, 50, 30);
    drawMidpointLine(-40, 220, 50, 180);
    drawBresenhamLine(50, RESOLUTION + 50, 200, RESOLUTION + 100);
    drawMidpointLine(RESOLUTION + 20, 10, RESOLUTION + 50, 200);
}

void drawClippedPolygon() {
    std::vector<Vector2f> polygon;

    polygon.emplace_back(Vector2f(-25, 125));
    polygon.emplace_back(Vector2f(125, 150));
    polygon.emplace_back(Vector2f(300, 110));
    polygon.emplace_back(Vector2f(25, 50));

    /*polygon.emplace_back(50, 50);
    polygon.emplace_back(150, 300);
    polygon.emplace_back(260, 400);
    polygon.emplace_back(180, 80);*/

    std::vector<Vector2f> clippedPolygon = calculateClippedPolygonCorners(polygon);

    for (int i = 0; i < clippedPolygon.size(); i++) {
        const Vector2f& start = clippedPolygon[i];
        const Vector2f& end = clippedPolygon[i < clippedPolygon.size() - 1 ? i + 1 : 0];

        std::cout << "Point " << i << ": (" << start(0) << ", " << start(1) << ")" << std::endl;
        drawMidpointLine(start(0), start(1), end(0), end(1));
    }
}

int main(int argc, char *argv[]) {
    setupGlut(&argc, argv, "[float] Exercise 4", 1024, 1024);

    std::string selectedExercise{ };
    while (true) {
        std::cout << "Please select an exercise:" << std::endl;
        std::cout << "1a - Line clipping (Cohan-Sutherland)" << std::endl;
        std::cout << "1b - Polygon clipping (Sutherland-Hodgman)" << std::endl;
        std::cout << "2a - Rasterize triangle with colors based on the barycentric coordinates" << std::endl;

        std::cout << "Selection: ";
        std::cin >> selectedExercise;

        if (selectedExercise == "1a") {
            setDrawingColor(1, 0, 1);

            demonstrateCohanSutherland();

            resetDrawingColor();
            break;
        }
        else if (selectedExercise == "1b") {
            setDrawingColor(0, 1, 1);

            drawClippedPolygon();

            resetDrawingColor();
            break;
        }
        else if (selectedExercise == "2a") {
            rasterizeBarycentricTriangle(Vector2f(50, 50), Vector2f(200, 50), Vector2f(125, 180));
            break;
        }
    }

    glutMainLoop();

    return 0;
}