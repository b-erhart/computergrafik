# Vertiefung MI: Computergrafik 

Meine Lösungen zu den Übungen aus der Medieninformatik (MI) - Computergrafik Vertiefung an der FHWS.

## Overview

| Blatt | Status         | Kommentar                               |
|:-----:|:---------------|:----------------------------------------|
| 1     | fertig         | -                                       |
| 2     | fertig         | -                                       |
| 3     | fertig         | -                                       |
| 4     | fertig         | -                                       |
| 5     | offen          | -                                       |