//
// Created by b-erhart on 03.04.21.
//

#ifndef COMPUTERGRAPHICS_B_SPLINE_CURVE_HPP
#define COMPUTERGRAPHICS_B_SPLINE_CURVE_HPP

#include <iostream>
#include "../simple2d.hpp"
#include "../types/coordinates.hpp"
#include "../exercise01/midpoint_line.hpp"

int mathematicalModulo(int a, int b) {
    return ((a % b) + b) % b;
}

std::vector<double> generateClosedKnotVector(const unsigned int &n, const unsigned int &m) {
    std::vector<double> knotVector{ };
    const double spacing = 1.0 / (m + 1);

    knotVector.emplace_back(0.0);
    for (int i{ 1 }; i <= m + 1; ++i) {
        knotVector.emplace_back(knotVector.at(i - 1) + spacing);
    }

    for (int i = m + 2; i <= n + m + 1; ++i) {
        const unsigned int j{ i - (m + 1) };
        knotVector.emplace_back(knotVector.at(i - 1) + (knotVector.at(j) - knotVector.at(j - 1)));
    }

    return knotVector;
}

Coordinate2d<double> calculateDeBoor(std::vector<Coordinate2d<double>> controlPoints, std::vector<double> nodeVector, int n, double t) {
    Coordinate2d<double> b[nodeVector.size()][nodeVector.size()];
    int i{ -1 };

    for (int k{ 0 }; k < nodeVector.size() - 1; ++k) {
        if (nodeVector[k] <= t && nodeVector[k + 1] > t) {
            i = k;
            break;
        }

        if (k == nodeVector.size() - 2) {
            std::cout << "DrawBSplineCurve: Invalid value \"t = " << t;
            std::cout << "\" for node vector" << std::endl;
            return Coordinate2d<double>{ };
        }
    }

    for (int j{ 0 }; j <= n; ++j) {
        for (int l{ i - n + j }; l <= i; ++l) {
            if (j == 0) {
                b[l][0] = controlPoints[l];
            }
            else {
                double t2 = (t - nodeVector[l]) / (nodeVector[l + n + 1 - j] - nodeVector[l]);
                b[l][j].x = (1 - t2) * b[l - 1][j - 1].x + t2 * b[l][j - 1].x;
                b[l][j].y = (1 - t2) * b[l - 1][j - 1].y + t2 * b[l][j - 1].y;
            }
        }
    }

    return b[i][n];
}

Coordinate2d<double> calculateDeBoorClosed(const std::vector<Coordinate2d<double>> &controlPoints, const std::vector<double> &nodeVector, const unsigned int &n, double t) {
    Coordinate2d<double> b[controlPoints.size()][controlPoints.size()];
    int i{ 0 };
    int m{ static_cast<int>(controlPoints.size() - 1) };

    for (int k{ 0 }; k <= m; ++k) {
        if (nodeVector[k] <= t && nodeVector[k + 1] > t) {
            i = k;
            break;
        }

        if (k == m) {
            std::cout << "DrawBSplineCurve: Invalid value \"t = " << t;
            std::cout << "\" for node vector" << std::endl;
            return Coordinate2d<double>{ };
        }
    }


    for (int j{ 0 }; j <= n; ++j) {
        auto l = static_cast<int>(i - n + j - 1);

        do {
            l = l + 1;

            if (l < 0) {
                l = l + m + 1;
                t = t - nodeVector[0] + nodeVector[m + 1];
            }
            else if (l >= m + 1) {
                l = l - m - 1;
                t = t - nodeVector[m + 1] + nodeVector[0];
            }

            if (j == 0) {
                b[l][0] = controlPoints[l];
            }
            else {
                double t2 = (t - nodeVector[l]) / (nodeVector[l + n + 1 - j] - nodeVector[l]);

                const int idx = mathematicalModulo(l - 1, m + 1); // (l - 1) % (m + 1);
                b[l][j].x = (1.0 - t2) * b[idx][j - 1].x + t2 * b[l][j - 1].x;
                b[l][j].y = (1.0 - t2) * b[idx][j - 1].y + t2 * b[l][j - 1].y;
            }
        } while(l != i);
    }

    return b[i][n];
}

template <typename T>
void drawControlPointsBorder(const std::vector<Coordinate2d<T>> &controlPoints) {
    RgbPixel previousDrawingColor = getDrawingColor();
    setDrawingColor(0.2, 0.2, 0.2);

    for (int i = 0; i < controlPoints.size(); i++) {
        if (i != controlPoints.size() - 1) {
            drawMidpointLine(controlPoints.at(i).x, controlPoints.at(i).y, controlPoints.at(i + 1).x, controlPoints.at(i + 1).y);
        }
        else {
            drawMidpointLine(controlPoints.at(i).x, controlPoints.at(i).y, controlPoints.at(0).x, controlPoints.at(0).y);
        }
    }

    setDrawingColor(previousDrawingColor);
}

void drawClosedBSpline(const std::vector<Coordinate2d<double>> &controlPoints, const unsigned int &n) {
    const auto m = static_cast<unsigned int>(controlPoints.size() - 1);

    const std::vector<double> knotVector = generateClosedKnotVector(n, m);
    std::vector<Coordinate2d<double>> linePoints{ };

    drawControlPointsBorder(controlPoints);

    for (double l{ 0 }; l < 1.0; l += 0.05) {
        linePoints.push_back(calculateDeBoorClosed(controlPoints, knotVector, n, l));
    }

    for (Coordinate2d<double> currentPoint : linePoints) {
        setPixel(std::round(currentPoint.x), std::round(currentPoint.y), GLfloat(0), GLfloat(1), GLfloat(1));
    }
}

#endif // #ifndef COMPUTERGRAPHICS_B_SPLINE_CURVE_HPP
