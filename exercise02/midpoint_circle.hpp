//
// Created by b-erhart on 25.03.21.
//

#ifndef COMPUTERGRAPHICS_MIDPOINT_CIRCLE_HPP
#define COMPUTERGRAPHICS_MIDPOINT_CIRCLE_HPP

#include "../simple2d.hpp"

void drawMidpointCircle(int x, int y, int radius) {
    int x1{ 0 };
    int y1{ radius };
    int f{ 1 - radius };
    int dx{ 3 };
    int dy{ 2 - 2 * radius };

    while (x1 <= y1) {
        setPixel(x + x1, y - y1);
        setPixel(x + x1, y + y1);
        setPixel(x - x1, y - y1);
        setPixel(x - x1, y + y1);
        setPixel(x + y1, y - x1);
        setPixel(x + y1, y + x1);
        setPixel(x - y1, y - x1);
        setPixel(x - y1, y + x1);

        x1++;

        if (f > 0) {
            y1--;
            f += dy;
            dy += 2;
        }

        f += dx;
        dx += 2;
    }
}

#endif // #ifndef COMPUTERGRAPHICS_MIDPOINT_CIRCLE_HPP
