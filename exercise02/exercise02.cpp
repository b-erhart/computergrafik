//
// Created by b-erhart on 24.03.21.
//

#include <vector>
#include <cmath>

#include "../simple2d.hpp"
#include "midpoint_circle.hpp"
#include "bezier_curve.hpp"
#include "b_spline_curve.hpp"

void midpointCircle() {
    setDrawingColor(1, 1, 0.5);

    drawMidpointCircle(128, 128, 50);
    drawMidpointCircle(128, 128, 5);

    resetDrawingColor();
}

void bezierCurve() {
    std::vector<Coordinate2d<int>> controlPoints;
    std::vector<Coordinate2d<int>> bezierLinePoints;

    Coordinate2d<int> a{ 50, 100 };
    Coordinate2d<int> b{ 70, 250 };
    Coordinate2d<int> c{ 230, 230 };
    Coordinate2d<int> d{ 180, 100 };

    setDrawingColor(0.2, 0.2, 0.2);
    drawMidpointLine(a.x, a.y, b.x, b.y);
    drawMidpointLine(b.x, b.y, c.x, c.y);
    drawMidpointLine(c.x, c.y, d.x, d.y);

    controlPoints.push_back(a);
    controlPoints.push_back(b);
    controlPoints.push_back(c);
    controlPoints.push_back(d);

    for (double i{ 0 }; i <= 1; i += 0.03)
        bezierLinePoints.push_back(calculateDeCasteljau(controlPoints, i));

    setDrawingColor(1, 0.35, 0);

    for (int k{ 0 }; k < bezierLinePoints.size(); ++k)
        setPixel(0.5 + bezierLinePoints[k].x, 0.5 + bezierLinePoints[k].y);

    resetDrawingColor();
}

void bSplineCurve() {
    std::vector<Coordinate2d<double>> controlPoints;

    std::vector<double> bSplineNodeVector;
    std::vector<Coordinate2d<double>> bSplineLinePoints;

    Coordinate2d<double> a{ 50, 100 };
    Coordinate2d<double> b{ 70, 250 };
    Coordinate2d<double> c{ 230, 230 };
    Coordinate2d<double> d{ 180, 100 };
    Coordinate2d<double> e{ 160, 80 };
    Coordinate2d<double> f{ 120, 70 };

    setDrawingColor(0.2, 0.2, 0.2);
    drawMidpointLine(a.x, a.y, b.x, b.y);
    drawMidpointLine(b.x, b.y, c.x, c.y);
    drawMidpointLine(c.x, c.y, d.x, d.y);
    drawMidpointLine(d.x, d.y, e.x, e.y);
    drawMidpointLine(e.x, e.y, f.x, f.y);

    controlPoints.push_back(a);
    controlPoints.push_back(b);
    controlPoints.push_back(c);
    controlPoints.push_back(d);
    controlPoints.push_back(e);
    controlPoints.push_back(f);

    bSplineNodeVector.push_back(0);
    bSplineNodeVector.push_back(0);
    bSplineNodeVector.push_back(0);
    bSplineNodeVector.push_back(0.25);
    bSplineNodeVector.push_back(0.5);
    bSplineNodeVector.push_back(0.75);
    bSplineNodeVector.push_back(1);
    bSplineNodeVector.push_back(1);
    bSplineNodeVector.push_back(1);

    for (double l{ 0 }; l < 1; l += 0.05) {
        bSplineLinePoints.push_back(calculateDeBoor(controlPoints, bSplineNodeVector, 2, l));
    }

    setDrawingColor(1, 0.35, 0);

    for (Coordinate2d<double> currentPoint : bSplineLinePoints) {
        setPixel(std::round(currentPoint.x), std::round(currentPoint.y));
    }

    resetDrawingColor();
}

void bSplineCurveClosed() {
    std::vector<Coordinate2d<double>> controlPoints{ };

    controlPoints.emplace_back(Coordinate2d<double>{ 50, 100 });
    controlPoints.emplace_back(Coordinate2d<double>{ 70, 250 });
    controlPoints.emplace_back(Coordinate2d<double>{ 230, 230 });
    controlPoints.emplace_back(Coordinate2d<double>{ 180, 100 });
    controlPoints.emplace_back(Coordinate2d<double>{ 160, 80 });
    controlPoints.emplace_back(Coordinate2d<double>{ 120, 70 });

    setDrawingColor(1, 0.35, 0);

    drawClosedBSpline(controlPoints, 3);

    resetDrawingColor();
}

int main(int argc, char *argv[]) {
    setupGlut(&argc, argv, "[float] Exercise 2", 1024, 1024);

    std::string selectedExercise{ };
    while (true) {
        std::cout << "Please select an exercise:" << std::endl;
        std::cout << "1  - Midpoint circle" << std::endl;
        std::cout << "3  - Bezier curve (de Casteljau)" << std::endl;
        std::cout << "4a - Open B-Spline curve (de Boor)" << std::endl;
        std::cout << "4b - Closed B-Spline curve (de Boor)" << std::endl;

        std::cout << "Selection: ";
        std::cin >> selectedExercise;

        if (selectedExercise == "1") {
            midpointCircle();
            break;
        }
        else if (selectedExercise == "3") {
            bezierCurve();
            break;
        }
        else if (selectedExercise == "4a") {
            bSplineCurve();
            break;
        }
        else if (selectedExercise == "4b") {
            bSplineCurveClosed();
            break;
        }

        std::cout << R"(Invalid selection. Please type either "1", "3", "4a" or "4b")" << std::endl << std::endl;
    }

    glutMainLoop();

    return 0;
}