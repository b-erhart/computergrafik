//
// Created by b-erhart on 25.03.21.
//

#ifndef COMPUTERGRAPHICS_BEZIER_CURVE_HPP
#define COMPUTERGRAPHICS_BEZIER_CURVE_HPP

#include <iostream>
#include "../types/coordinates.hpp"

Coordinate2d<int> calculateDeCasteljau(std::vector<Coordinate2d<int>> bezierPoints, double t) {
    Coordinate2d<int> b[bezierPoints.size()][bezierPoints.size()];

    for (int i{ 0 }; i < bezierPoints.size(); ++i) {
        b[i][0] = bezierPoints[i];
    }

    for (int j{ 1 }; j < bezierPoints.size(); ++j) {
        for (int k{ j }; k < bezierPoints.size(); ++k) {
            b[k][j].x = (1 - t) * b[k - 1][j - 1].x + t * b[k][j - 1].x;
            b[k][j].y = (1 - t) * b[k - 1][j - 1].y + t * b[k][j - 1].y;
        }
    }

    return b[bezierPoints.size() - 1][bezierPoints.size() - 1];
}

#endif // #ifndef COMPUTERGRAPHICS_BEZIER_CURVE_HPP
